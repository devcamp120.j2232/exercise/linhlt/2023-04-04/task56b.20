package com.devcamp.bookauthorapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookAuthorApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookAuthorApiApplication.class, args);
	}

}
