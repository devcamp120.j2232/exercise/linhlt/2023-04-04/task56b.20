package com.devcamp.bookauthorapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.Author;
import com.devcamp.bookauthorapi.Book;

@RestController
@RequestMapping("/")
@CrossOrigin
public class BookController {
    @GetMapping("/books")
    public ArrayList<String> booksApi(){
        //task 4
        Author author1 = new Author("Victor Hugo", "victor@yahoo.com", 'm');
        Author author2 = new Author("Marie Joy", "marie@gmail.com", 'f');
        Author author3 = new Author("Linh Lai", "linh@gmail.com", 'f');
        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);
        //task 5
        Book book1 = new Book("How to be good", author1, 120000, 5);
        Book book2 = new Book("Beauty and the beast", author2, 80000, 2);
        Book book3 = new Book("Calligraphy and life", author3, 90000, 3);
        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);
        //task 6
        ArrayList<String> bookList = new ArrayList<>();
        bookList.add(book1.toString());
        bookList.add(book2.toString());
        bookList.add(book3.toString());

        return bookList;

    }
}
